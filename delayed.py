""" This module provides the Delayed class

Delayed is intended to be inherited from.  A class that inherits class can
specify with the class attribute __delayed_attributes__ attributes that will
not be written to directly. Instead all changes will take effect when calling
the commit method.

To be able to read a value that is not yet commited the `future` property can
be used.
To write a value and commit it immediatly the `now` property can be used
"""
import types
import weakref
from collections import ChainMap
from collections import abc
from copy import deepcopy
from typing import List  # pylint: disable=unused-import


class Delayed:
    """ class that implements the delayed writes and future and now accessors
    """

    __delayed_attributes__ = []  # type: List[str]

    def __init__(self):
        self._delayed_setattrs = {}
        self._delayed_lambdas = []

    def __setattr__(self, name, value):
        if name not in vars(self):
            object.__setattr__(self, name, value)
            return
        if name not in self.__delayed_attributes__:
            object.__setattr__(self, name, value)
            return
        # self._delayed_setattrs[name] = deepwrap(value, self)
        if isinstance(value, abc.MutableMapping):
            self._delayed_setattrs[name] = deepwrap(value, self)
        elif isinstance(value, abc.MutableSequence):
            self._delayed_setattrs[name] = deepwrap(value, self)
        elif hasattr(value, "__dict__"):
            self._delayed_setattrs[name] = deepwrap(value, self)
        else:
            self._delayed_setattrs[name] = deepcopy(value)

    def commit(self):
        """ Commit all delayed changes """
        self.__dict__.update(self._delayed_setattrs)
        self.__dict__["_delayed_setattrs"] = {}
        for delayed_func in self._delayed_lambdas:
            delayed_func()

    def peek(self, name):
        """ Peek into changes that will happen after commit """
        return ChainMap(self._delayed_setattrs, self.__dict__)[name]

    @property
    def now(self):
        """ Write values bypassing delay """
        delayed_instance = self

        class Now:  # pylint: disable=too-few-public-methods
            """Wrapper to allow writing to delayed attributes without delay"""

            def __setattr__(self, name, value):
                # pylint: disable=protected-access
                if name in delayed_instance._delayed_setattrs:
                    # pylint: enable=protected-access
                    raise AttributeError(
                        "{name} will be overwritten after commit".format(name=name)
                    )
                delayed_instance.__dict__[name] = value

            def __getattribute__(self, name):
                raise AttributeError("Cannot read via now")

        return Now()

    @property
    def future(self):
        """ Read values as if they were committed """
        delayed_instance = self

        class Future:  # pylint: disable=too-few-public-methods
            """Wrapper to allow reading to delayed
               attributes as if committed"""

            def __getattribute__(self, name):
                return delayed_instance.peek(name)

            def __setattr__(self, name, value):
                raise AttributeError("Cannot write into future")

        return Future()


def _deepwrap_atomic(wrappee, delayed_instance):  # pylint: disable=unused-argument
    return wrappee


def deepwrap(wrappee, delayed_instance):
    """ Recursively wrap an object """
    wrap_dispatcher = {}
    wrap_dispatcher[type(None)] = _deepwrap_atomic
    wrap_dispatcher[type(Ellipsis)] = _deepwrap_atomic
    wrap_dispatcher[type(NotImplemented)] = _deepwrap_atomic
    wrap_dispatcher[int] = _deepwrap_atomic
    wrap_dispatcher[float] = _deepwrap_atomic
    wrap_dispatcher[bool] = _deepwrap_atomic
    wrap_dispatcher[complex] = _deepwrap_atomic
    wrap_dispatcher[bytes] = _deepwrap_atomic
    wrap_dispatcher[str] = _deepwrap_atomic
    wrap_dispatcher[types.CodeType] = _deepwrap_atomic
    wrap_dispatcher[type] = _deepwrap_atomic
    wrap_dispatcher[types.BuiltinFunctionType] = _deepwrap_atomic
    wrap_dispatcher[types.FunctionType] = _deepwrap_atomic
    wrap_dispatcher[weakref.ref] = _deepwrap_atomic
    wrap_dispatcher[list] = deepwrap_list
    wrap_dispatcher[dict] = deepwrap_dict

    cls = type(wrappee)
    wrapper = wrap_dispatcher.get(cls)
    if wrapper is None:
        if hasattr(wrappee, "__dict__"):
            return deepwrap_object(wrappee, delayed_instance)
        raise NotImplementedError("No wrapper function for {cls}".format(cls=cls))

    return wrapper(wrappee, delayed_instance)


def deepwrap_list(lst, delayed_instance):
    """ Wrap a list so all changes to it are captured and can applied later """
    new_lst = []
    for element in lst:
        new_lst.append(deepwrap(element, delayed_instance))
    return SequenceWrapper(new_lst, delayed_instance)


def deepwrap_dict(dct, delayed_instance):
    """ Wrap a dict so all changes to it are captured and can applied later """
    new_dct = {}
    for key, value in dct.items():
        new_dct[key] = deepwrap(value, delayed_instance)
    return MappingWrapper(new_dct, delayed_instance)


def deepwrap_object(obj, delayed_instance):
    """ Wrap an object so all changes to it are captured and can applied later """
    new_obj = deepcopy(obj)
    for attr_name, attr in obj.__dict__.items():
        setattr(new_obj, attr_name, deepwrap(attr, delayed_instance))
    return ObjectWrapper(new_obj, delayed_instance)


class MappingWrapper(abc.MutableMapping):
    """ Wrapper around mappings to delay updates """

    # pylint: disable=protected-access

    def __init__(self, value, delayed_instance):
        super().__init__()
        self._wrapped = value
        self._delayed_instance = delayed_instance

    def __setitem__(self, name, value):
        self._delayed_instance._delayed_lambdas.append(
            lambda name=name, value=value, setitem=self._wrapped.__setitem__: setitem(
                name, value
            )
        )

    def __delitem__(self, name):
        self._delayed_instance._delayed_lambdas.append(
            lambda name=name, delitem=self._wrapped.__delitem__: delitem(name)
        )

    def __getitem__(self, name):
        return self._wrapped[name]

    def __len__(self):
        return len(self._wrapped)

    def __iter__(self):
        return iter(self._wrapped)


class SequenceWrapper(abc.MutableSequence):  # pylint: disable=too-many-ancestors
    """ Wrapper around sequences to delay updates """

    # pylint: disable=protected-access

    def __init__(self, value, delayed_instance):
        super().__init__()
        self._wrapped = value
        self._delayed_instance = delayed_instance

    def __setitem__(self, name, value):
        self._delayed_instance._delayed_lambdas.append(
            lambda name=name, value=value, setitem=self._wrapped.__setitem__: setitem(
                name, value
            )
        )

    def __delitem__(self, name):
        self._delayed_instance._delayed_lambdas.append(
            lambda name=name, delitem=self._wrapped.__delitem__: delitem(name)
        )

    def insert(self, index, value):
        print("insert {value} at {index}".format(index=index, value=value))
        self._delayed_instance._delayed_lambdas.append(
            lambda index=index, value=value, _insert=self._wrapped.insert: _insert(
                index, value
            )
        )

    def __getitem__(self, name):
        return self._wrapped[name]

    def __len__(self):
        return len(self._wrapped)

    def __eq__(self, other):
        return self._wrapped == other


class ObjectWrapper:  # pylint: disable=too-few-public-methods
    """ Wrapper around sequences to delay updates """

    # pylint: disable=protected-access
    __initialized__ = False

    def __init__(self, value, delayed_instance):
        super().__init__()
        self._wrapped = value
        self._delayed_instance = delayed_instance
        self.__initialized__ = True

    def __setattr__(self, name, value):
        if self.__initialized__:
            self._delayed_instance._delayed_lambdas.append(
                lambda name=name, value=value, setattr_=self._wrapped.__setattr__: setattr_(
                    name, value
                )
            )
        else:
            super().__setattr__(name, value)

    def __getattr__(self, name):
        return getattr(self._wrapped, name)

    def __delattr__(self, name):
        return delattr(self._wrapped, name)
