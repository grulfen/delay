# pylint: disable=missing-docstring, invalid-name, redefined-outer-name
import pytest
from delayed import Delayed


class WithDelays(Delayed):
    __delayed_attributes__ = ["slow", "another_slow"]

    def __init__(self):
        super().__init__()
        self.slow = None
        self.another_slow = None
        self.fast = None


@pytest.fixture
def delayed_object():
    return WithDelays()


def test_write_slow_does_not_update_directly(delayed_object):
    delayed_object.slow = 12
    assert delayed_object.slow is None


def test_written_values_applies_after_running_apply(delayed_object):
    delayed_object.slow = 12
    delayed_object.commit()
    delayed_object.slow = 13
    assert delayed_object.slow == 12


def test_fast_is_updated_directly(delayed_object):
    delayed_object.fast = 10
    assert delayed_object.fast == 10
    delayed_object.commit()
    assert delayed_object.fast == 10


def test_peek_value(delayed_object):
    delayed_object.slow = 12
    delayed_object.fast = 13
    assert delayed_object.peek("slow") == 12
    assert delayed_object.peek("fast") == 13


def test_write_via_now(delayed_object):
    delayed_object.now.slow = 12
    assert delayed_object.slow == 12


def test_pending_write_are_not_commited_when_using_now(delayed_object):
    delayed_object.slow = 1
    delayed_object.now.another_slow = 12
    assert delayed_object.slow is None
    delayed_object.commit()
    assert delayed_object.slow == 1


def test_writing_via_now_when_pending_update_raises(delayed_object):
    delayed_object.slow = 1
    with pytest.raises(AttributeError):
        delayed_object.now.slow = 12


def test_writing_via_slow_is_overwritten_by_delayed_write(delayed_object):
    delayed_object.now.slow = 12
    delayed_object.slow = 1
    delayed_object.commit()
    assert delayed_object.slow == 1


def test_write_via_future_raises(delayed_object):
    with pytest.raises(AttributeError):
        delayed_object.future.slow = 12


def test_read_via_future(delayed_object):
    delayed_object.slow = 12
    assert delayed_object.future.slow == 12
    assert delayed_object.slow is None


def test_read_via_now_raises(delayed_object):
    delayed_object.slow = 12
    delayed_object.commit()
    with pytest.raises(AttributeError) as exc:
        delayed_object.now.slow  # pylint: disable=pointless-statement
    assert str(exc.value) == "Cannot read via now"


def test_dicts_are_copied(delayed_object):
    a_dict = {1: 2}
    delayed_object.slow = a_dict
    delayed_object.commit()
    a_dict[1] = 3
    assert delayed_object.slow[1] == 2


def test_change_via_dict_is_delayed(delayed_object):
    delayed_object.slow = {1: 2}
    delayed_object.commit()
    delayed_object.slow[1] = 3
    assert delayed_object.slow[1] == 2
    delayed_object.commit()
    assert delayed_object.slow[1] == 3


def test_can_read_delayed_dict_write_via_future(delayed_object):
    delayed_object.slow = {1: 2}
    delayed_object.commit()
    delayed_object.slow[1] = 3
    assert delayed_object.future.slow[1] == 3
    assert delayed_object.slow[1] == 2


def test_update_via_dict_is_delayed(delayed_object):
    delayed_object.slow = {1: 2}
    delayed_object.commit()
    delayed_object.slow.update({1: 3, 2: 4})
    assert delayed_object.slow == {1: 2}
    delayed_object.commit()
    assert delayed_object.slow == {1: 3, 2: 4}


def test_delete_dictitem_is_delayed(delayed_object):
    delayed_object.slow = {1: 2, 2: 3}
    delayed_object.commit()
    del delayed_object.slow[1]
    assert delayed_object.slow == {1: 2, 2: 3}
    delayed_object.commit()
    assert delayed_object.slow == {2: 3}


def test_change_via_list_is_delayed(delayed_object):
    delayed_object.slow = [1, 2]
    delayed_object.commit()
    delayed_object.slow.append(3)
    assert delayed_object.slow == [1, 2]
    delayed_object.commit()
    assert delayed_object.slow == [1, 2, 3]


def test_plusequal_does_not_update(delayed_object):
    delayed_object.slow = 12
    delayed_object.commit()
    delayed_object.slow += 1
    assert delayed_object.slow == 12
    delayed_object.commit()
    assert delayed_object.slow == 13


def test_change_via_dict_in_dict_is_delayed(delayed_object):
    delayed_object.slow = {1: {2: 3}}
    delayed_object.commit()
    delayed_object.slow[1][2] = 4
    assert delayed_object.slow[1][2] == 3
    delayed_object.commit()
    assert delayed_object.slow[1][2] == 4


def test_objects_in_slow_are_delayed(delayed_object):
    class SomeClass:
        def __init__(self, some_attr):
            self.some_attr = some_attr

    some_obj = SomeClass(12)
    delayed_object.slow = some_obj
    delayed_object.commit()
    delayed_object.slow.some_attr = 14
    assert delayed_object.slow.some_attr == 12
    delayed_object.commit()
    assert delayed_object.slow.some_attr == 14


def test_wrapped_objects_can_access_wrapped_objects_methods(delayed_object):
    class SomeClass:
        def __init__(self, some_attr):
            self.some_attr = some_attr

        def tjosan(self):
            return 123

    some_obj = SomeClass(12)
    delayed_object.slow = some_obj
    delayed_object.commit()
    assert delayed_object.slow.tjosan() == 123


def test_dict_in_class_is_delayed(delayed_object):
    class SomeClass:
        def __init__(self, some_attr):
            self.some_attr = some_attr

    some_obj = SomeClass(12)
    some_obj.some_attr = {1: 2}
    delayed_object.slow = some_obj
    delayed_object.commit()
    delayed_object.slow.some_attr[1] = 3
    assert delayed_object.slow.some_attr[1] == 2
    delayed_object.commit()
    assert delayed_object.slow.some_attr[1] == 3
